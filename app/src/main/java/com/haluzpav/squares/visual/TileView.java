package com.haluzpav.squares.visual;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import com.haluzpav.squares.core.Tile;

@SuppressLint("ViewConstructor")
public class TileView extends View {

    private static final Paint paintActive = new Paint();
    private static final Paint paintPassive = new Paint();
    static {
        paintActive.setARGB(100, 255, 0, 0);
        paintPassive.setARGB(100, 0, 0, 255);
    }

    public Tile tile;

    public TileView(Context context) {
        super(context);
    }

    public TileView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TileView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public TileView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawCircle(5, 10, 20, paintActive);
        Paint tilePaint = tile != null && tile.active ? paintActive : paintPassive;
        canvas.drawPaint(tilePaint);
    }

}
