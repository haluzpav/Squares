package com.haluzpav.squares.visual;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.haluzpav.squares.BuildConfig;
import com.haluzpav.squares.R;
import com.haluzpav.squares.core.Board;
import com.haluzpav.squares.core.Tile;

public class BoardLayout extends LinearLayout {

    private Board board;

    public BoardLayout(Context context) {
        super(context);
        init();
    }

    public BoardLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BoardLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        // just to for designer to display something
        if (BuildConfig.DEBUG) {
            setBoard(new Board(4, 4));
        }
    }

    public void click(TileView view) {
        Tile tile = view.tile;
        board.click(tile);
        view.invalidate();
        for (Tile neighbor : board.getNeighbors(tile)) {
            getTileViewFrom(neighbor.i).invalidate();
        }
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board boardNew) {
        final boolean newViews = (board == null ^ boardNew == null) ||
                (board != null && boardNew != null && !board.isSameSize(boardNew));
        board = boardNew;
        if (newViews) {
            createTileViews();
        }
        updateTileShortcuts();
        invalidateTiles();
        if (newViews) {
            requestLayout();
        }
    }

    private void createTileViews() {
        removeAllViews();
        setOrientation(VERTICAL);

        LayoutInflater inflater = LayoutInflater.from(getContext());
        LinearLayout rowLayout;
        LinearLayout.LayoutParams rowParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1
        );
        LinearLayout.LayoutParams tileParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1
        );

        for (int r = 0; r < board.height; r++) {

            rowLayout = new LinearLayout(getContext());
            rowLayout.setOrientation(HORIZONTAL);

            for (int c = 0; c < board.width; c++) {
                View tile = inflater.inflate(R.layout.item_tile_stretchy, this, false);
                tile.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        BoardLayout.this.click(getTileViewFrom(v));
                    }
                });
                rowLayout.addView(tile, tileParams);
            }

            addView(rowLayout, rowParams);
        }
    }

    private void updateTileShortcuts() {
        for (int i = 0; i < board.size(); i++) {
            TileView tileView = getTileViewFrom(i);
            tileView.tile = board.get(i);
        }
    }

    private void invalidateTiles() {
        for (int i = 0; i < board.size(); i++) {
            getTileViewFrom(i).invalidate();
        }
    }

    private TileView getTileViewFrom(View parent) {
        return (TileView) parent.findViewById(R.id.tileView);
    }

    private TileView getTileViewFrom(int position) {
        LinearLayout row = (LinearLayout) getChildAt(position / board.width);
        View tile = row.getChildAt(position % board.width);
        return getTileViewFrom(tile);
    }

}
