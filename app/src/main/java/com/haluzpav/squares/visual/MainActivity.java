package com.haluzpav.squares.visual;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.haluzpav.squares.R;
import com.haluzpav.squares.core.Board;
import com.haluzpav.squares.preferences.MyPreferenceActivity;

import java.io.FileInputStream;
import java.io.FileOutputStream;

public class MainActivity extends AppCompatActivity {

    private static String saveFileName = "board.ser";
    private static String cacheFileName = "board_cache.ser";

    private SharedPreferences prefs;
    private BoardLayout boardLayout;
    private OnMyPreferenceListener onMyPreferenceListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        boardLayout = (BoardLayout) findViewById(R.id.boardLayout);
        assert boardLayout != null;
        createBoard();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_load:
                loadBoard(false);
                return true;
            case R.id.action_save:
                saveBoard(false);
                return true;
            case R.id.action_settings:
                showPreferences();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        // TODO better non-persistent saving (use Bundle?)
        // don't judge me, lol
        saveBoard(true);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // TODO same as onSaveInstanceState
        loadBoard(true);
    }

    private void createBoard() {
        final int width = prefs.getInt(getString(R.string.pref_key_width), getResources().getInteger(R.integer.pref_default_size));
        final int height = prefs.getInt(getString(R.string.pref_key_height), getResources().getInteger(R.integer.pref_default_size));
        boardLayout.setBoard(new Board(width, height));
    }

    private void saveBoard(boolean asCache) {
        // TODO exceptions
        // TODO async
        try {
            FileOutputStream fos = openFileOutput(asCache ? cacheFileName : saveFileName, Context.MODE_PRIVATE);
            boardLayout.getBoard().save(fos);
            fos.close();
            if (!asCache) {
                Toast.makeText(this, R.string.toast_save_ok, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            String text = String.format(getString(R.string.toast_save_fail), e.getClass().getSimpleName());
            Toast.makeText(this, text, Toast.LENGTH_LONG).show();
        }
    }

    private void loadBoard(boolean fromCache) {
        // TODO exceptions
        // TODO async
        try {
            FileInputStream fis = openFileInput(fromCache ? cacheFileName : saveFileName);
            boardLayout.setBoard(Board.load(fis));
            fis.close();
            if (!fromCache) {
                Toast.makeText(this, R.string.toast_load_ok, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            String text = String.format(getString(R.string.toast_load_fail), e.getClass().getSimpleName());
            Toast.makeText(this, text, Toast.LENGTH_LONG).show();
        }
    }

    private void showPreferences() {
        Intent intent = new Intent(this, MyPreferenceActivity.class);
        startActivity(intent);
        // can't declare listener here because it might be garbage-collected
        if (onMyPreferenceListener == null) {
            onMyPreferenceListener = new OnMyPreferenceListener();
        }
        prefs.registerOnSharedPreferenceChangeListener(onMyPreferenceListener);
    }

    public class OnMyPreferenceListener implements SharedPreferences.OnSharedPreferenceChangeListener {
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
                                              String key) {
            if (key.equals(getString(R.string.pref_key_width)) || key.equals(getString(R.string.pref_key_height))) {
                // TODO confirm delete current board dialog
                MainActivity.this.createBoard();
            }
        }
    }

}
