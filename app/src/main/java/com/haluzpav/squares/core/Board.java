package com.haluzpav.squares.core;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Board extends ArrayList<Tile> implements Serializable {

    public int width;
    public int height;

    public Board(int width, int height) {
        super(width * height);
        this.width = width;
        this.height = height;
        for (int i = 0; i < width * height; i++) {
            Tile tile = new Tile(this, i);
            add(tile);
        }
    }

    public void click(Tile tile) {
        tile.flip();
        for (Tile neighbor : getNeighbors(tile)) {
            neighbor.flip();
        }
    }

    public List<Tile> getNeighbors(Tile tile) {
        List<Tile> neighbors = new ArrayList<>(4);
        appendEdging(neighbors, tile, 1);
        appendEdging(neighbors, tile, width);
        return neighbors;
    }

    private boolean isInGrid(int a) {
        return 0 <= a && a < size();
    }

    private void appendEdging(List<Tile> list, Tile center, int distance) {
        for (int dir = -1; dir <= 1; dir += 2) {
            int i = center.i + dir * distance;
            if (!isInGrid(i)) {
                continue;
            }
            Tile edge = get(i);
            if (center.inLineWith(edge, width)) {
                list.add(edge);
            }
        }
    }

    public void updateIndexes() {
        for (Tile tile : this) {
            tile.i = indexOf(tile);
        }
    }

    public boolean isSameSize(Board other) {
        return size() == other.size();
    }

    public void save(FileOutputStream fos) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(this);
        oos.close();
    }

    static public Board load(FileInputStream fis) throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(fis);
        Board board = (Board) ois.readObject();
        ois.close();
        return board;
    }

}
