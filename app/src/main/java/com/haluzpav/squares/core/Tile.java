package com.haluzpav.squares.core;

import java.io.Serializable;

public class Tile implements Serializable {

    public Board board;
    public int i;

    public boolean active = false;

    public Tile(Board board, int i) {
        this.board = board;
        this.i = i;
    }

    public void flip() {
        active = !active;
    }

    public boolean inLineWith(Tile other, int widthGrid) {
        return i / widthGrid == other.i / widthGrid ||
                Math.abs(i - other.i) == widthGrid;
    }

}
