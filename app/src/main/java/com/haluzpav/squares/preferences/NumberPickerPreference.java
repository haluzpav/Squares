package com.haluzpav.squares.preferences;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.NumberPicker;

import com.haluzpav.squares.R;

public class NumberPickerPreference extends DialogPreference {

    // TODO major generalization needed

    private static final int value_default = 0;

    private int value;
    protected NumberPicker numberPicker;

    public NumberPickerPreference(Context context, AttributeSet attrs) {
        super(context, attrs);

        setDialogLayoutResource(R.layout.dialog_preference_numberpicker);
        setPositiveButtonText(android.R.string.ok);
        setNegativeButtonText(android.R.string.cancel);

        setDialogIcon(null);
    }

    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);
        numberPicker = (NumberPicker) view.findViewById(R.id.numberPicker);
        Resources res = getContext().getResources();
        numberPicker.setMinValue(res.getInteger(R.integer.pref_min_size));
        numberPicker.setMaxValue(res.getInteger(R.integer.pref_max_size));
        numberPicker.setValue(value);
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {
            value = numberPicker.getValue();
            persistInt(value);
        }
    }

    @Override
    protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue) {
        if (restorePersistedValue) {
            value = this.getPersistedInt(value_default);
        } else {
            value = (Integer) defaultValue;
            persistInt(value);
        }
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return a.getInteger(index, value_default);
    }

    // TODO onSaveInstanceState() and onRestoreInstanceState()

}
