package com.haluzpav.squares.preferences;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import com.haluzpav.squares.R;

public class MyPreferenceFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);
    }

}
